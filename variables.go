package main

import (
	"regexp"
	"time"

	"github.com/jinzhu/gorm"
)

var (
	mobRegexp = regexp.MustCompile("^[0-9]{4,13}$")
	db        *gorm.DB
)

const (
	expiredSession = "Session Expired. Please login again..."
)

type (
	response map[string]interface{}

	//Sessions is struct for gorm model to create sessions table
	Sessions struct {
		gorm.Model
		Mobile        string    `gorm:"type:text;column:mobile"`
		SessionID     string    `gorm:"type:text;column:session_id"`
		UserAgent     string    `gorm:"type:text;column:user_agent"`
		IP            string    `gorm:"type:text ; column: ip"`
		GeneratedTime time.Time `gorm:"column:generated_time"`
	}

	//User is struct for gorm model to create user details table
	User struct {
		gorm.Model
		UserName         string    `gorm:"type:text ; not null;column:user_name"`
		Mobile           string    `gorm:"type:text;column:mobile"`
		CurrentSessionID string    `gorm:"type:text;column: current_session_id "`
		GeneratedTime    time.Time `gorm:"column:generated_time"`
	}
)
