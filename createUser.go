package main

import (
	"net"
	"net/http"
	"time"

	guuid "github.com/google/uuid"
)

//createUser is http handler func for createUser endpoint
//validates form , sets SessionID cookie
//and responds with sessionID as json payload
func createUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		respondErr(w, response{
			"error_type": "Form data Parse Error",
			"error":      err.Error(),
		})
		return
	}
	userName := r.FormValue("username")
	mobile := r.FormValue("mobile")
	if userName == "" || mobile == "" {

		// sessionIDs []string
		w.WriteHeader(http.StatusBadRequest)
		respondErr(w, response{
			"error_type": "Form Data Error",
			"error": "Empty Form values. Make sure to use urlencoded form data :	'x-www-form-urlencoded'",
		})
		return
	}
	if !mobRegexp.MatchString(mobile) {
		w.WriteHeader(http.StatusBadRequest)
		respondErr(w, response{
			"error_type": "Mobile format Error",
			"error":      "Please check mobile number format. Make sure it is just numbers without countrycode or any symbols or space",
		})
		return
	}
	userAgent := r.UserAgent()
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	sessionID, err := userValidate(userName, mobile, userAgent, ip)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		respondErr(w, response{
			"error_type": "DB Query Error",
			"error":      err.Error(),
		})
	}
	http.SetCookie(w, &http.Cookie{
		Name:    "SessionID",
		Value:   sessionID,
		Expires: time.Now().AddDate(0, 0, 1),
	})
	respondOK(w, response{
		"cookie_name": "SessionID",
		"value":       sessionID,
	})
}

//userValidate generates new session id and inserts into sessions table
//it also inserts or update user table with new and current sessionid
func userValidate(userName, mobile, userAgent, ip string) (string, error) {
	sessionID := guuid.New().String()
	currentTime := time.Now()
	var user User
	err := db.Where(User{Mobile: mobile, UserName: userName}).
		Assign(User{CurrentSessionID: sessionID, GeneratedTime: currentTime}).
		FirstOrCreate(&user).Error
	if err != nil {
		return "", err
	}

	err = db.Create(&Sessions{Mobile: mobile,
		SessionID:     sessionID,
		UserAgent:     userAgent,
		IP:            ip,
		GeneratedTime: currentTime,
	}).Error
	if err != nil {
		return "", err
	}
	return sessionID, nil
}
