package main

import (
	"encoding/json"
	"net/http"
)

//response when status ok (200)
func respondOK(w http.ResponseWriter, data interface{}) {
	w.WriteHeader(http.StatusOK)
	resp, _ := json.Marshal(response{"data": data})
	w.Write(resp)
}

//reponse when there is error
func respondErr(w http.ResponseWriter, data interface{}) {
	resp, _ := json.Marshal(response{"meta": data})
	w.Write(resp)
}
