package main

import (
	"errors"
	"net/http"
)

//getUser is http handler function for getUser endpoint
//validates cookie 'SessionID' and 'mobile' parmater
//responds with user details and all sessions
func getUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	sessionID, err := r.Cookie("SessionID")
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		respondErr(w, response{
			"error_type": "Auth error",
			"error":      "Auth Cookie 'SessionID' not found. Please login...",
		})
		return
	}
	mobile := r.URL.Query().Get("mobile")
	if len(mobile) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		respondErr(w, response{
			"error_type": "Parameter Missing",
			"error":      "'mobile' parameter missing",
		})
		return
	}
	if !mobRegexp.MatchString(mobile) {
		w.WriteHeader(http.StatusBadRequest)
		respondErr(w, response{
			"error_type": "Mobile format Error",
			"error":      "Please check mobile number format. Make sure it is just numbers without countrycode or any symbols or space",
		})
		return
	}
	details, err := getUserOps(sessionID.Value, mobile)
	if err != nil {
		if err.Error() == expiredSession || err.Error() == "record not found" {
			w.WriteHeader(http.StatusUnauthorized)
			respondErr(w, response{
				"error_type": "Unauthorized",
				"error":      expiredSession,
			})
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		respondErr(w, response{
			"error_type": "DB Query Error",
			"error":      err.Error(),
		})
		return
	}
	respondOK(w, details)
}

//getUserOps fetches user details and sessions from DB
func getUserOps(sessionID, mobile string) (response, error) {
	var (
		user            User
		sessionsDetails []struct {
			Session string `gorm:"column:session_id"  json:"session"`
		}
	)
	err := db.Where(&User{Mobile: mobile}).First(&user).Error
	if err != nil {
		return nil, err
	}
	if sessionID != user.CurrentSessionID {
		return nil, errors.New(expiredSession)
	}

	err = db.Table("sessions").Select([]string{"session_id"}).
		Where(Sessions{Mobile: mobile}).Find(&sessionsDetails).Error
	if err != nil {
		return nil, err
	}
	return response{
		"mobile_number": mobile,
		"user_name":     user.UserName,
		"activeSession": user.CurrentSessionID,
		"sessions":      sessionsDetails,
	}, nil

}
