package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func initiateDB() {
	var err error
	db, err = gorm.Open("sqlite3", "./db.sqlite")
	if err != nil {
		log.Fatal(err)
	}
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Sessions{})
}

func main() {
	address := flag.String("addr", ":8080", "address to listen on")
	flag.Parse()
	initiateDB()
	defer db.Close()
	router := http.NewServeMux()
	router.HandleFunc("/createUser", createUser)
	router.HandleFunc("/doGetUser", getUser)

	fmt.Println("Routes:")
	fmt.Println("Endpoint: ", "/createUser", " Method: POST")
	fmt.Println("Endpoint:", "/doGetUser", " Method: GET")
	fmt.Println("Starting server on :", *address)
	log.Fatalln(http.ListenAndServe(*address, router))
}
