# Sleeping Banker Problem [(Finin - Hiring problem - SDE)](https://letsfinin.gitbook.io/hiring-problem-software-developer-backend/)

- This program is coded using golang.
- Sqlite3 has been used as database
- For session maintenance cookie has been used
- Endpoints
    - /createUser - POST
    - /doGetUser - GET

# How to build

- [Install golang](https://golang.org/dl)
- ```go get ./...``` - to install dependencies
- ```go build``` - to build then run the binary
- To run directly - ```go run *.go```
- Make sure cgo is enabled as sqlite driver is c port.

